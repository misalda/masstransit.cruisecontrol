﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using Autofac;
using GreenPipes;
using MassTransit;
using MassTransit.AutofacIntegration;
using MassTransit.Azure.ServiceBus.Core;
using MassTransit.MessageData;
using Microsoft.Azure.ServiceBus.Primitives;
using Microsoft.Extensions.Logging;
using MTCC.Common.Configuration;
using MTCC.Common.Services;
using MTCC.Conventions;

namespace MTCC.Autofac
{
    public static class ServiceBusRegistrationExtensions
    {
        public static void AddServiceBus(this ContainerBuilder builder,Action<IContainerBuilderConfigurator> configure)
        {
            builder.AddMassTransit(configure);
        }

        public static void AddServiceBus(this ContainerBuilder builder, BusConfiguration busConfiguration, ILogger logger = null, IMessageDataRepository messageRepository = null)
        {
            builder.AddMassTransit(x =>
            {
                var map = busConfiguration.TopologyMap;
                foreach (var consumer in map.EventConsumers.SelectMany(i => i.ConsumerClassTypes))
                {
                    x.AddConsumer(consumer);
                    logger?.LogInformation("Adding Event Consumer {ConsumerName}", consumer.FullName);
                }
                foreach (var consumer in map.MessageConsumers.SelectMany(i => i.ConsumerClassTypes))
                {
                    x.AddConsumer(consumer);
                    logger?.LogInformation("Adding Event Consumer {ConsumerName}", consumer.FullName);
                }
                foreach (var consumer in map.RequestConsumers.SelectMany(i => i.ConsumerClassTypes))
                {
                    x.AddConsumer(consumer);
                    logger?.LogInformation("Adding Event Consumer {ConsumerName}", consumer.FullName);
                }

                x.AddBus(provider => Bus.Factory.CreateUsingAzureServiceBus(cfg =>
                {
                    cfg.RequiresSession = false;
                    cfg.MaxConcurrentCalls = 500;
                    cfg.MessageWaitTimeout = TimeSpan.FromMinutes(5);

                    var host = cfg.Host(new Uri(busConfiguration.AzureServiceBus.Uri), h =>
                    {
                        h.OperationTimeout = TimeSpan.FromSeconds(5);
                        h.SharedAccessSignature(s =>
                        {
                            s.KeyName = busConfiguration.AzureServiceBus.KeyName;
                            s.SharedAccessKey = busConfiguration.AzureServiceBus.SharedAccessKey;
                            s.TokenTimeToLive = TimeSpan.FromDays(1);
                            s.TokenScope = TokenScope.Namespace;
                        });
                    });
                    logger?.LogInformation("connected to azure service-bus namespace {namespace}", busConfiguration.AzureServiceBus.Uri);
                    SetupSubscriptionEndpoints(map.DefaultEndPoint, provider.Container, cfg, map.EventConsumers, host,logger,messageRepository);
                    SetupQueueEndpoints(provider.Container, cfg, map.MessageConsumers, host,logger, messageRepository);
                    SetupQueueEndpoints(provider.Container, cfg, map.RequestConsumers, host,logger, messageRepository);
                    if (messageRepository != null)
                    {
                        cfg.UseMessageData(messageRepository);
                    }
                }));

                var methodInfo = typeof(IRegistrationConfigurator).GetMethod("AddRequestClient", new[] { typeof(Uri), typeof(RequestTimeout) });
                foreach (var endpoint in map.RequestClientEndpoints)
                {
                    MethodInfo genericMethod = methodInfo?.MakeGenericMethod(endpoint.Key);
                    genericMethod?.Invoke(x, new object[] { new Uri($"{busConfiguration.AzureServiceBus.Uri}/{endpoint.Value}"), RequestTimeout.After(m: 10) });
                }
            });
            builder.RegisterInstance(busConfiguration);
            builder.RegisterType<EnterpriseBusService>().As<IEnterpriseBus>();
        }

        private static void SetupQueueEndpoints(IComponentContext provider, IServiceBusBusFactoryConfigurator cfg, IEnumerable<ConsumerEntry> consumerEntries, IServiceBusHost host, ILogger logger, IMessageDataRepository messageRepository)
        {

            foreach (var entry in consumerEntries)
            {
                if (!entry.ConsumerClassTypes.Any())
                    continue;

                logger?.LogInformation("Configuring queue {queueName} to handle contract {contractName}", entry.ReceiveEndPoint, entry.ContractClassType);

                cfg.ReceiveEndpoint(entry.ReceiveEndPoint, c =>
                {
                    c.EnableDeadLetteringOnMessageExpiration = true;
                    c.ConfigureConsumeTopology = false;
                    c.MaxDeliveryCount = 3;
                    c.UseMessageRetry(r => r.Exponential(5, TimeSpan.FromSeconds(5), TimeSpan.FromSeconds(30), TimeSpan.FromSeconds(1)));
                    c.ConfigureConsumer(provider, entry.ConsumerClassTypes.ToArray());
                });
            }
        }

        private static void SetupSubscriptionEndpoints(string defaultNameSpace, IComponentContext provider, IServiceBusBusFactoryConfigurator cfg, IEnumerable<ConsumerEntry> topologyEntries, IServiceBusHost host, ILogger logger, IMessageDataRepository messageRepository)
        {
            foreach (var entry in topologyEntries)
            {
                if (!entry.ConsumerClassTypes.Any())
                    continue;

                logger?.LogInformation("Configuring subscription {subscription} on topic {receiveEndPoint} to handle contract {contractName}", defaultNameSpace, entry.ReceiveEndPoint, entry.ContractClassType);

                cfg.SubscriptionEndpoint( defaultNameSpace, entry.ReceiveEndPoint, c =>
                {
                    c.EnableDeadLetteringOnMessageExpiration = true;
                    c.MaxDeliveryCount = 3;
                    c.UseMessageRetry(r => r.Exponential(5, TimeSpan.FromSeconds(5), TimeSpan.FromSeconds(30), TimeSpan.FromSeconds(1)));
                    c.ConfigureConsumer(provider,entry.ConsumerClassTypes.ToArray());
                });
            } 
        }

    }
}
