﻿using Microsoft.Extensions.Hosting;

namespace MTCC.Common.Services
{
    public interface IEnterpriseBus : ISendMessages, IPublishMessages, IHostedService
    {
        string Name { get; }
    }
}
