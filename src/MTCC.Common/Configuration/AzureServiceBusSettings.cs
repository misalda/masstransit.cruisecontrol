﻿namespace MTCC.Common.Configuration
{
    public class AzureServiceBusSettings
    {
        public string Uri { get; set; }
        public string KeyName { get; set; }
        public string SharedAccessKey { get; set; }
    }
}
