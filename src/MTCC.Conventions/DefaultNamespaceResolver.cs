﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Runtime.CompilerServices;
using GreenPipes.Internals.Extensions;
using MassTransit;
using MTCC.Conventions.ContractAttributes;

namespace MTCC.Conventions
{
    public class DefaultNamespaceResolver : INamespaceResolver
    {
        private const string RootNamespace = "MTCC";
        private readonly string _rootNamespace;
        private readonly string _contractNameSpace;
        private readonly string _defaultEndpointName;
        private readonly IEnumerable<Assembly> _assemblies;

        public DefaultNamespaceResolver(string rootNameSpace = null, string defaultEndpointName = null)
        {
            _rootNamespace = rootNameSpace ?? RootNamespace;
            _defaultEndpointName = defaultEndpointName ?? GenerateDefaultEndpoint();
            LoadAssemblies();
            //get all the relevant assemblies
            _assemblies = AppDomain.CurrentDomain.GetAssemblies().Where(a => a.FullName.StartsWith(_rootNamespace, StringComparison.InvariantCultureIgnoreCase));

        }
        private string GenerateDefaultEndpoint()
        {
            try
            {
                return Assembly.GetEntryAssembly()?.DefinedTypes.First().Namespace;
            }
            catch
            {
                return $"{Guid.NewGuid()}";
            }

        }
        public string GetEndpointPath(Type type)
        {

            var contactType = type.GetAttribute<ContractTypeAttribute>().First()?.Type;
            return contactType == ContractType.Event ? $"{type.Namespace}/{type.Name}".ToLower() : $"{type.FullName}".ToLower();
        }
        private void LoadAssemblies()
        {
            string directoryName = Path.GetDirectoryName(Assembly.GetExecutingAssembly().Location);
            var files = Directory.GetFiles(directoryName ?? throw new InvalidOperationException(), $"{_rootNamespace}*").Where(f => f.EndsWith(".dll"));
            foreach (string assemblyFile in files)
            {
                try
                {
                    Assembly.LoadFrom(assemblyFile);
                }
                catch
                {
                    continue;
                    //ignore assembly file
                }
            }
        }
        public TopologyMap BuildTopology()
        {
            //map each contract with its consumer             
            var topologyMap = new TopologyMap()
            {
                EventConsumers = GetConsumerEntries(ContractType.Event),
                MessageConsumers = GetConsumerEntries(ContractType.Message),
                RequestConsumers = GetConsumerEntries(ContractType.Request),
                SendEndpoints = GetClientEndpoints(ContractType.Message),
                RequestClientEndpoints = GetClientEndpoints(ContractType.Request),
                DefaultEndPoint = _defaultEndpointName
            };
            return topologyMap;
        }
        private IList<ConsumerEntry> GetConsumerEntries(ContractType contractType)
        {
            //get all the types that define contracts according to the Contract Types
            IList<Type> messageTypes = GetContractTypes(contractType);

            var consumerEntries = new List<ConsumerEntry>(0);
            foreach (var messageType in messageTypes)
            {
                var entry = new ConsumerEntry() { ContractClassType = messageType };
                entry.ConsumerClassTypes = _assemblies.SelectMany((assembly) => GetLoadableTypes(assembly)).Where(dt => dt.GetInterfaces().Any(gi => gi.IsGenericType && gi.GetGenericTypeDefinition().Equals(typeof(IConsumer<>)) && gi.UnderlyingSystemType.GetGenericArguments()[0] == messageType));
                entry.ReceiveEndPoint = GetEndpointPath(messageType);
                consumerEntries.Add(entry);
            }
            return consumerEntries;
        }
        private IReadOnlyDictionary<Type, string> GetClientEndpoints(ContractType contractType)
        {
            //get all the types that define contracts according to the DefaultContract Types
            IList<Type> messageTypes = GetContractTypes(contractType);

            var endpointEntries = new Dictionary<Type, string>(0);
            foreach (var messageType in messageTypes)
            {
                endpointEntries.Add(messageType, GetEndpointPath(messageType));
            }

            return endpointEntries;
        }
        private IList<Type> GetContractTypes(ContractType contractType)
        {
            return (from type in _assemblies.SelectMany((assembly) => GetLoadableTypes(assembly))
                    where type.GetAttribute<ContractTypeAttribute>().Any() && type.GetAttribute<ContractTypeAttribute>().First().Type == contractType
                    select type).ToList();
        }
        public IEnumerable<Type> GetLoadableTypes(Assembly assembly)
        {
            // Algorithm from StackOverflow answer here:
            // https://stackoverflow.com/questions/7889228/how-to-prevent-reflectiontypeloadexception-when-calling-assembly-gettypes
            if (assembly == null) throw new ArgumentNullException(nameof(assembly));

            try
            {
                return assembly.DefinedTypes.Select(t => t.AsType());
            }
            catch (ReflectionTypeLoadException ex)
            {
                var result = ex.Types.Where(type => type != null &&
                Attribute.IsDefined(type, typeof(CompilerGeneratedAttribute), false)
                && type.IsGenericType && type.Name.Contains("AnonymousType")
                && (type.Name.StartsWith("<>") || type.Name.StartsWith("VB$"))
                && type.Attributes.HasFlag(TypeAttributes.NotPublic));
                return result;
            }
        }
    }
}
