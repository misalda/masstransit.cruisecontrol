﻿using System.ComponentModel;

namespace MTCC.Conventions.ContractAttributes
{
    public enum ContractType
    {
        [Description("Event")]
        Event,
        [Description("Message")]
        Message,
        [Description("Request")]
        Request
    }
}
