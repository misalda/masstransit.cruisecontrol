﻿using System;

namespace MTCC.Conventions.ContractAttributes
{
    [AttributeUsage(AttributeTargets.Interface | AttributeTargets.Class, AllowMultiple = false)]
    public class EventContractAttribute : ContractTypeAttribute
    {
        public EventContractAttribute():base(ContractType.Event)
        {
            
        }
    }
}