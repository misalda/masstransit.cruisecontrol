﻿using System;

namespace MTCC.Conventions.ContractAttributes
{
    [AttributeUsage(AttributeTargets.Interface | AttributeTargets.Class, AllowMultiple = false)]
    public class RequestContractAttribute : ContractTypeAttribute
    {
        public RequestContractAttribute() : base(ContractType.Request)
        {
            
        }
    }
}