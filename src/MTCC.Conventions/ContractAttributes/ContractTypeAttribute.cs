﻿using System;

namespace MTCC.Conventions.ContractAttributes
{
    public abstract class ContractTypeAttribute : Attribute
    {
        private readonly ContractType _type;
        public ContractTypeAttribute(ContractType type)
        {
            _type = type;
        }

        public ContractType Type => _type;
    }
}