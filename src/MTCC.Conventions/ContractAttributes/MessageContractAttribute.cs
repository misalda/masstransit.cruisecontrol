﻿using System;

namespace MTCC.Conventions.ContractAttributes
{
    [AttributeUsage(AttributeTargets.Interface | AttributeTargets.Class, AllowMultiple = false)]
    public class MessageContractAttribute : ContractTypeAttribute
    {
        public MessageContractAttribute() : base(ContractType.Message)
        {
           
        }
    }
}