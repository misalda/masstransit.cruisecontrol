﻿namespace MTCC.Conventions
{
    public interface INamespaceResolver
    {
        TopologyMap BuildTopology();
    }
}