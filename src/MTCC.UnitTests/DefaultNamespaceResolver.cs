using System.Linq;
using EnterpriseBus.UnitTests.Consumers;
using EnterpriseBus.UnitTests.Events;
using EnterpriseBus.UnitTests.Messages;
using EnterpriseBus.UnitTests.Requests;
using MTCC.Conventions;
using NUnit.Framework;

namespace MTCC.UnitTests
{
    public class DefaultNameSpaceResolverTests
    {
        private TopologyMap _map;
        [OneTimeSetUp]
        public void Setup()
        {
            _map = new DefaultNamespaceResolver().BuildTopology();
        }

        [Test]
        public void TestEventConsumersEntriesCreated()
        {
            Assert.IsTrue(_map.EventConsumers.Count() == 2, "event consumer entries  created");
           
        }
        [Test]
        public void TestEvent2MultipleConsumersClassesCreated()
        {
            Assert.IsTrue(_map.EventConsumers.Single(i=> i.ContractClassType == typeof(Test1Event)).ConsumerClassTypes.Count() == 2, $"one event consumer entries  created for { typeof(Test1Event).Name} with 2 consumers");
        }
        [Test]
        public void TestRequestConsumersEntriesCreated()
        {
            Assert.IsTrue(_map.RequestConsumers.Count() == 2, "request consumer entries  created");

        }
        [Test]
        public void TestRequestConsumersClassEntriesCreated()
        {
            Assert.IsTrue(_map.RequestConsumers.Single(i => i.ContractClassType == typeof(Test1Request)).ConsumerClassTypes.Count() == 1, $"one request consumer entries  created for { typeof(Test1Request).Name} with 1 consumer");
            Assert.IsTrue(_map.RequestConsumers.Single(i => i.ContractClassType == typeof(Test1Request)).ConsumerClassTypes.Single() == typeof(Test1RequestProcessor), $"consumer for { typeof(Test1Request).Name} set to expected type");
        }
        [Test]
        public void TestMessageConsumersEntriesCreated()
        {
            Assert.IsTrue(_map.MessageConsumers.Count() == 1, "event consumer entries  created");

        }
        [Test]
        public void TestMessageConsumerClassesEntriesCreated()
        {
            Assert.IsTrue(_map.MessageConsumers.Single(i => i.ContractClassType == typeof(Test1Message)).ConsumerClassTypes.Count() == 1, $"one event Message entries  created for { typeof(Test1Message).Name} with 1 consumer");
            Assert.IsTrue(_map.MessageConsumers.Single(i => i.ContractClassType == typeof(Test1Message)).ConsumerClassTypes.Single() == typeof(Test1MessageProcessor), $"consumer for { typeof(Test1Message).Name} set to expected type");
        }
    }
}