﻿using EnterpriseBus.UnitTests.Consumers;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;
using Moq;
using MTCC.Common.Configuration;
using MTCC.Conventions;
using MTCC.NetCoreDI;
using NUnit.Framework;

namespace MTCC.UnitTests
{
    [TestFixture]
    public class CoreRegistrationExtensionsTests
    {
        
        private ILogger _logger;
        private AzureServiceBusSettings _sbSettings;
        [OneTimeSetUp]
        public void Setup()
        {
            _logger =new Mock<ILogger>().Object;
            _sbSettings = new AzureServiceBusSettings() { Uri = "https://localhost:8080", KeyName = "KeyName", SharedAccessKey = "Accesskey" };


        }
        [Test]
        public void Consumers_are_registered_as_expected()
        {
            //create service collection
            var services = new ServiceCollection();
            //call registration code with mock settings and test econsumers and contracts
            services.AddServiceBus(new BusConfiguration(new DefaultNamespaceResolver(),_sbSettings), _logger);
            var provider = services.BuildServiceProvider();
            Assert.IsNotNull(provider.GetService<Test1EventProcessor>());
            Assert.IsNotNull(provider.GetService<Test1EventProcessor2>());
            Assert.IsNotNull(provider.GetService<Test2EventProcessor>());
            Assert.IsNotNull(provider.GetService<Test1MessageProcessor>());
            Assert.IsNotNull(provider.GetService<Test1RequestProcessor>());
            Assert.IsNotNull(provider.GetService<Test1RequestProcessor>());

        }
    }

}
