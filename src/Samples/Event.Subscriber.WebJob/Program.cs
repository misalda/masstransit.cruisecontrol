﻿using System.Reflection;
using System.Threading.Tasks;
using MassTransit.MessageData;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Logging;
using Microsoft.WindowsAzure.Storage;
using MTCC.Common.Configuration;
using MTCC.Common.Infrastructure;
using MTCC.Common.Services;
using MTCC.NetCoreDI;
using Serilog;
using Serilog.Events;
using Serilog.Extensions.Logging;
using Serilog.Formatting.Json;

namespace Event.Subscriber.WebJob
{
    class Program
    {
        static async Task Main(string[] args)
        {
            Assembly assembly = Assembly.GetEntryAssembly();
            Log.Logger = new LoggerConfiguration()
           .MinimumLevel.Information()
           .MinimumLevel.Override("Microsoft", LogEventLevel.Warning)
           .Enrich.WithProperty("Application", assembly.GetName().Name)
           .Enrich.WithProperty("Runtime", assembly.ImageRuntimeVersion)
           .Enrich.FromLogContext()
           .WriteTo.RollingFile(new JsonFormatter(), "Logs-{Date}.log", shared: true)
           .WriteTo.Console()
           .CreateLogger();

            var host = new HostBuilder()
                .UseEnvironment("Development")
                .ConfigureWebJobs(b =>
                {
                    b.AddAzureStorageCoreServices()
                    .AddAzureStorage()
                    .AddServiceBus();
                })
                .ConfigureAppConfiguration(b =>
                {
                    // Adding command line as a configuration source
                    b.AddJsonFile("appsettings.json");
                })
                .ConfigureServices((context, services) =>
                {
                    var storageAccount = CloudStorageAccount.Parse(context.Configuration.GetConnectionString("ServiceBusStorageAccount"));
                    services.AddSingleton(storageAccount);

                    var serviceCollection = services.BuildServiceProvider();
                    var blobMessageDataRepositoryLogger = serviceCollection.GetRequiredService<ILogger<BlobMessageDataRepository>>();

                    var blobMessageRepository = new BlobMessageDataRepository(
                        storageAccount,
                        blobMessageDataRepositoryLogger
                    );
                    services.AddTransient<IMessageDataRepository>(sp => blobMessageRepository);

                    var asbConfig = new BusConfiguration(azureServiceBusSettings: new AzureServiceBusSettings()
                    {
                        Uri = context.Configuration["AzureSbNameSpace"],
                        KeyName = context.Configuration["AzureSbKeyName"],
                        SharedAccessKey = context.Configuration["AzureSbSharedAccessKey"]
                    });

                    var logger = new SerilogLoggerProvider(Log.Logger).CreateLogger(nameof(Program));

                    services.AddServiceBus(asbConfig, logger,blobMessageRepository);
                    services.AddHostedService<EnterpriseBusService>();
                })
                .UseSerilog()
                .Build();

            await host.RunAsync();
        }
    }
}
