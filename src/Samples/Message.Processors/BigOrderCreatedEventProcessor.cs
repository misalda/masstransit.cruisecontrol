﻿using System;
using System.IO;
using System.Threading.Tasks;
using MassTransit;
using MTCC.Sample.Contracts.Events;

namespace MTCC.Message.Processors
{
    public class BigOrderCreatedEventProcessor : IConsumer<BigOrderCreatedEvent>
    {
        public async Task Consume(ConsumeContext<BigOrderCreatedEvent> context)
        {
            if (context.Message.BigData.HasValue) {
                var myValue = await context.Message.BigData.Value;
                using (var stream = new MemoryStream(myValue))
                using (var reader = new StreamReader(stream))
                {
                    var text = reader.ReadToEnd();
                    Console.WriteLine(text);
                }
            }

            // update the customer address
        }
    }
}