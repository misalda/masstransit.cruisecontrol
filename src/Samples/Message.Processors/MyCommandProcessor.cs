﻿using System;
using System.Threading.Tasks;
using MassTransit;
using MTCC.Sample.Contracts.Messages;

namespace MTCC.Message.Processors
{
    public class MyCommandProcessor : IConsumer<MyMessage>
    {
        public async Task Consume(ConsumeContext<MyMessage> context)
        {
            await Console.Out.WriteLineAsync($"Command says: {context.Message.MessageText}");

            // update the customer address

        }
    }
}