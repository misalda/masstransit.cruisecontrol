﻿using System;
using MassTransit;
using MTCC.Conventions.ContractAttributes;

namespace MTCC.Sample.Contracts.Events
{
    [EventContract]
    public interface BigOrderCreatedEvent
    {
        Guid OrderId { get; }
        string OrderDescription { get; }
        DateTime Timestamp { get; }
        DateTime OrderCreatedDateTime { get; }
        MessageData<byte[]> BigData { get; }
    }
}