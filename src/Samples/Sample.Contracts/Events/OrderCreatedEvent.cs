﻿using System;
using MTCC.Conventions.ContractAttributes;

namespace MTCC.Sample.Contracts.Events
{
    [EventContract]
    public interface OrderCreatedEvent
    {
        Guid OrderId { get; }
        string OrderDescription { get; }
        DateTime Timestamp { get; }
        DateTime OrderCreatedDateTime { get; }
    }
}