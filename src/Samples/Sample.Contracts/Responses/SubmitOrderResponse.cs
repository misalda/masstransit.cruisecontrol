﻿using System;

namespace MTCC.Sample.Contracts.Responses
{
    public interface SubmitOrderResponse
    {
        string OrderId { get; }
        string OrderDescription { get; }
        DateTime CreationTime { get; set; }
    }

}