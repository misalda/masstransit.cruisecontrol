﻿using MTCC.Conventions.ContractAttributes;

namespace MTCC.Sample.Contracts.Requests
{
    [RequestContract]
    public interface SubmitOrderRequest
    {
        string OrderDescription { get; }
    }

}