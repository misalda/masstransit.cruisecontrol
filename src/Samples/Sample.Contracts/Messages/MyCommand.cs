﻿using MTCC.Conventions.ContractAttributes;

namespace MTCC.Sample.Contracts.Messages
{
    [MessageContract]
    public interface MyMessage
    {
        string MessageText { get; set; }
    }

}