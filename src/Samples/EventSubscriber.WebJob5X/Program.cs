﻿using System.Configuration;
using System.Reflection;
using System.Threading;
using Autofac;
using Microsoft.Azure.WebJobs;
using MTCC.Autofac;
using MTCC.Common.Configuration;
using MTCC.Common.Services;
using Serilog;
using Serilog.Events;
using Serilog.Extensions.Autofac.DependencyInjection;
using Serilog.Extensions.Logging;
using Serilog.Formatting.Json;

namespace EventSubscriber.WebJob5X
{
    // To learn more about Microsoft Azure WebJobs SDK, please see https://go.microsoft.com/fwlink/?LinkID=320976
    class Program
    {
        // Please set the following connection strings in app.config for this WebJob to run:
        // AzureWebJobsDashboard and AzureWebJobsStorage
        static void Main()
        {

            Assembly assembly = Assembly.GetEntryAssembly();

            var logConfig = new LoggerConfiguration()
           .MinimumLevel.Information()
           .MinimumLevel.Override("Microsoft", LogEventLevel.Warning)
           .Enrich.WithProperty("Application", assembly.GetName().Name)
           .Enrich.WithProperty("Runtime", assembly.ImageRuntimeVersion)
           .Enrich.FromLogContext()
           .WriteTo.RollingFile(new JsonFormatter(), "Logs-{Date}.log", shared: true)
           .WriteTo.Console();


            var builder = new ContainerBuilder();
            var asbConfig = new BusConfiguration(azureServiceBusSettings: new AzureServiceBusSettings(){
                Uri = ConfigurationManager.AppSettings["AzureSbNameSpace"],
                KeyName = ConfigurationManager.AppSettings["AzureSbKeyName"],
                SharedAccessKey = ConfigurationManager.AppSettings["AzureSbSharedAccessKey"]
            });
            builder.RegisterSerilog(logConfig);

            var logger = new SerilogLoggerProvider().CreateLogger(nameof(Program));
            builder.AddServiceBus(asbConfig, logger);


            var container = builder.Build();
            var busControl = container.Resolve<IEnterpriseBus>();

            var config = new JobHostConfiguration
            {
                JobActivator = new AutofacActivator(container)
            };
            var host = new JobHost(config);

            busControl.StartAsync(default(CancellationToken));

            host.RunAndBlock();

            busControl.StopAsync(default(CancellationToken));
        }
    }
}
