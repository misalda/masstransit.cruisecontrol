# Mass Transit Cruise Control

MassTransit Cruise Control is a bootstrap library for Masstransit, A free, open-source distributed application framework for.NET (https://masstransit-project.com/getting-started/). 

This library defines conventions and naming rules to bootstrap the consumers and endpoints user by masstransit to setup queues, topic and subscriptions in Azure Service Bus to exchange messages by using reflection and class attributes.

The aim of this library is to provide a consistent way to setup endpoints and consumers on of a service bus 

3 types of contracts are defined

 - Events -> one to many via topics and subscriptions
- Messages -> one to one via queues
- request/responses -> via queues

the library sets extensions for two DI frameworks

- Autofac
- Net core DI

library is compiled against .net standard to provide support to both .net core and .net framework

project structure 

- Samples
	- Contracts -> Sample Contracts, Defines events messages and requests
	- Net.core 
		- Event producer: console application that produces messages
		- Event Subscriber Webjob: sample webjob that consumes messages produced by the producer
	- Net.Framework 
		- Event Subscriber Webjob: sample .net framework Web job that consumes messages
- MTCC.Autofac: autofac extensions
- MTCC.NetCoreDI: .net Core dependency injection extension methods
- MTCC.Conventions: this projects defines the define the conventions, naming rules and topology map that is ued by the DI extension project to bootstrap masstransit.
- MTCC.Common: this project defines the common services and configuration interfaces.
- MTCC.UnitTests: Tests!!

# How to try it out:
- Update appsetings.json and app.config files for projects under the samples folder to pint to an instance of Azure service Bus and a Storage Account.
- Start the Subscriber Webjobs under the samples folder
- Event producer is a command line application to run build the solution and then run:

dotnet Event.Producer.dll [options] [command]
 
		Options:

		  -v | --version
		  Show version information

		  -h | --help
		  Show help information


		Commands:

		  PublishBigEvent  Publish a Big event for all subscribers using Azure Blob storage
		  PublishEvent     Publish an event for all subscribers
		  SendMessage      Sends a message to a specific endpoint configure for the message type
		  SendRequest

		Use "dotnet Event.Producer.dll [command] --help" for more information about a command.
